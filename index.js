const cron = require('node-cron');
const { WebClient } = require('@slack/web-api');
const { createEventAdapter } = require('@slack/events-api');
const https = require('https');
const http = require('http');
const express = require('express');
const WATCHER_APP = 'https://jira-version-watcher.herokuapp.com/self/ping';
const dao = require("./dao");
const crawler = require("./scanners");


async function deleteSubscriber(subscriber) {
	await dao.deleteSubscriber(subscriber);
};

function equalsLatestEapsHashesIgnoreOrder(latestEaps, currentEaps) {
	latestEaps.sort();
	currentEaps.sort();
	if (currentEaps.length !== latestEaps.length) {
		return false;
	}
	for (var i = 0; i < currentEaps.length; i++) {
		if (currentEaps[i] !== latestEaps[i]) {
			return false;
		}
	}
	return true;
}

async function notifyBuilder() {
	let options = {
		hostname: 'www.postcatcher.in',
		port: 3333,
		path: '/buildLatest',
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		}
	};
	var req = http.request(options, function (res) {
		console.log('Status: ' + res.statusCode);
		console.log('Headers: ' + JSON.stringify(res.headers));
		res.setEncoding('utf8');
		res.on('data', function (body) {
			console.log('Body: ' + body);
		});
	});
	req.end();
}
async function notifyWatchers(reason) {
	let subscribers = await dao.readSubscribers();
	for (var i = 0; i < subscribers.length; i++) {
		console.log('Notifying: ', subscribers[i]);
		const res = web.chat.postMessage({ channel: subscribers[i], text: reason });
	}
}

function selfPing() {
	https.get(WATCHER_APP);
}

function selfPingResponse(req, res, next) {
	res.send('Alive');
}

async function checkEaps() {
	console.log('Getting latest JIRA Server version...');
	const newestEaps = crawler.scanEaps().map((eap) => eap.eap);
	const latestEaps = await getEaps();
	if (!equalsLatestEapsHashesIgnoreOrder(latestEaps, newestEaps)) {
		console.log('=============================================');
		console.log('Newer EAPS detected!');
		console.log('Notifying Molicki und SPG!');
		notifyWatchers('New JIRA Server EAP versions released! Go get\'em! \n ' + crawler.EAPS_URL);
		setEaps(newestEaps);
		console.log('Current hashes: ', newestEaps);
		console.log('=============================================')
	} else {
		console.log('No new JIRA Server EAP detected.');
	}
}

async function checkVersion() {
	console.log('Getting latest JIRA Server version...');
	const newestVersion = crawler.scanVersion();
	const latestVersion = await getVersion();
	if (newestVersion != latestVersion) {
		console.log('=============================================');
		console.log('Newer version detected!');
		console.log('Notifying Molicki und SPG!');
		notifyWatchers('New JIRA Server version released: ' + newestVersion + ', check it: ' + crawler.JIRA_SERVER_RELEASES);
		setVersion(newestVersion);
		console.log('Current version: ', newestVersion);
		console.log('=============================================')
	} else {
		console.log('No new JIRA Server version detected. Current is: ' + latestVersion);
	}
}

function initEventsHandler(events) {
	events.on('message', (event) => {
		if (event.user == null) {
			return;
		}
		switch (event.text) {
			case 'newest':
				respondWithNewestVersionInfo(event);
				break;
			case 'sub':
				addUserToSubscribers(event);
				break;
			case 'unsub':
				removeUserFromSubscribers(event);
				break;
			default:
				const res = web.chat.postMessage({ channel: event.channel, text: 'Me no speak bot-aliano' });
				break;
		}
	});
}

async function getEaps() {
	const result = await dao.fetchEaps();
	return result;
}

async function setEaps(eaps) {
	const result = await dao.setEaps(eaps);
}

async function getVersion() {
	const result = await dao.fetchVersion();
	return result;
}

async function setVersion(newestVersion) {
	const result = await dao.setVersion(newestVersion);
}

async function respondWithNewestVersionInfo(event) {
	const newestJiraVersion = await getVersion();
	const res = web.chat.postMessage({ channel: event.channel, text: 'Newest JIRA is: ' + newestJiraVersion });
}

async function addUserToSubscribers(event) {
	let sourceId = event.channel;
	const isUserSubscriber = await dao.isSubscriber(sourceId);
	if (!isUserSubscriber) {
		await dao.insertSubscriber(sourceId);
		const res = web.chat.postMessage({ channel: event.channel, text: 'Subscribed' });
	} else {
		const res = web.chat.postMessage({ channel: event.channel, text: 'Already subscribed' });
	}
}

async function showSubscribers(event) {
	let sourceId = event.channel;
	let subscribers = await dao.readSubscribers();
	const res = web.chat.postMessage({ channel: sourceId, text: subscribers });
}

async function removeUserFromSubscribers(event) {
	let sourceId = event.channel;
	await deleteSubscriber(sourceId);
	const res = web.chat.postMessage({ channel: event.channel, text: 'Unsubscribed' });
}

async function getLatestVersion(req, res, next) {
	const newestJiraVersion = await getVersion();
	res.send(newestJiraVersion);
}

async function getLatestVersionDownloadUrl(req, res, next) {
	const newestJiraVersion = await getVersion();
	let downloadUrl = 'https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-' + newestJiraVersion + '.tar.gz';
	res.send(downloadUrl);
}
// ------ MAIN -------
const app = express();
const slackEvents = createEventAdapter(process.env.SIGNING_SECRET);
const web = new WebClient(process.env.ACCESS_TOKEN);

app.use('/slack/events', slackEvents.expressMiddleware());
app.use('/self/ping', selfPingResponse);
app.get('/version/latest', getLatestVersion);
app.get('/version/latest/downloadUrl', getLatestVersionDownloadUrl);
app.listen(process.env.PORT, () => console.log(`Example app listening on port ${process.env.PORT}!`))
slackEvents.on('error', console.error);
initEventsHandler(slackEvents);

cron.schedule('* * * * *', () => {
	checkEaps();
	checkVersion();
});

cron.schedule('*/15 * * * *', () => {
	selfPing();
});
dao.init();
