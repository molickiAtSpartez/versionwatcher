export class Eap {
    eap: string;
    url: string;
    version: string;
    released: string;

    constructor(md5: string, url: string, version: string, released: string) {
        this.eap = md5;
        this.url = url;
        this.version = version;
        this.released = released;
    }
}
