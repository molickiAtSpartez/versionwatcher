export class Notification {
    constructor(public reason: string, public text: string) {
    }
}
