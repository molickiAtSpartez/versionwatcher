import { JiraEap } from "./JiraEap";
import request from "sync-request";
import { Eap } from "./Eap";

export class Scanner {
    public static JIRA_SERVER_RELEASES = 'https://confluence.atlassian.com/adminjiraserver/upgrade-matrix-966063322.html';
    public static JIRA_SERVER_VERSION = 'https://my.atlassian.com/download/feeds/current/jira-software.json';
    public static JIRA_EAPS = 'https://my.atlassian.com/download/feeds/eap/jira.json';
    public static EAPS_URL = 'https://www.atlassian.com/software/jira/download-eap';

    public scanVersion(): string {
        const version = this.jsonize(Scanner.JIRA_SERVER_VERSION);
        return version[0].version;
    }

    public scanEaps(): Eap[] {
        let eaps: JiraEap[] = this.jsonize(Scanner.JIRA_EAPS);
        let scannedEaps = [];
        eaps = eaps.filter(eap => eap.zipUrl.endsWith('.zip') && eap.zipUrl.includes('jira-software'));
        for (let i = 0; i < eaps.length; i++) {
            scannedEaps.push(new Eap(eaps[i].md5, eaps[i].zipUrl, eaps[i].version, eaps[i].released));
        }
        return scannedEaps;
    }

    private scrapDownloads(page: string): string {
        return page.substring('downloads('.length, page.length - 1);
    }

    private jsonize(url: string): JiraEap[] {
        const page = request('GET', url).getBody(('UTF-8'));
        const jsonString = this.scrapDownloads(page);
        return JSON.parse(jsonString);
    }
}
