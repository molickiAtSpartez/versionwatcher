import {Client, Result} from 'pg';

export class Dao {
    static client: Client = undefined;

    constructor() {
        if (this.getClient() === undefined) {
            Dao.client = new Client({
                connectionString: process.env.DATABASE_URL,
                ssl: true,
            });
            Dao.client.connect();
        }
    }

    public getClient(): Client {
        return Dao.client;
    }

    cleanTable(table) {
        try {
            this.getClient().query('BEGIN');
            try {
                this.getClient().query("DELETE FROM " + table + ";");
                this.getClient().query('COMMIT')
            } catch (err) {
                this.getClient().query('ROLLBACK');
                throw err;
            }
        } catch (e) {
            console.error(e);
        }
    }

    async select(q) {
        let res;
        res = await this.getClient().query(q);
        return res.rows;
    }

    handleDaoError(error: PromiseRejectionEvent) {
        console.log('error while executing statement... \n error is: ', error);
    }
}
