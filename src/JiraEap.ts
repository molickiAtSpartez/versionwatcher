export interface JiraEap {
    md5: string,
    zipUrl: string,
    version: string,
    released: string
}
