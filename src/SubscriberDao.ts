import {Dao} from "./Dao";

export class SubscriberDao extends Dao {

    insert(subscriber : string) {
        try {
            this.getClient().query('BEGIN');
            try {
                this.getClient().query('INSERT INTO subscribers (subscriber) VALUES(\'' + subscriber + '\');');
                this.getClient().query('COMMIT');
            } catch (err) {
                console.log('err: ', err);
                this.getClient().query('ROLLBACK');
                throw err;
            }
        } catch (e) {
            console.error(e);
        }
    }

    async getAll() {
        const res = await this.select('SELECT subscriber FROM subscribers;');

        return res.map((row) => row.subscriber);
    }

    isPresent(name : string) {
        const res = this.getClient().query('SELECT subscriber FROM subscribers WHERE subscriber=' + name + ' LIMIT 1;');
        return res.length == 1;
    }

    delete(subscriber : string) {
        try {
            this.getClient().query('BEGIN');
            try {
                this.getClient().query('DELETE FROM subscribers WHERE subscriber=\'' + subscriber + '\';');
                this.getClient().query('COMMIT');
            } catch (err) {
                this.getClient().query('ROLLBACK');
                throw err;
            }
        } catch (e) {
            console.error(e);
        }
    }
}
