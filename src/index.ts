import {Scanner} from "./Scanner";
import {EapDao} from "./EapDao";
import {VersionDao} from "./VersionDao";
import {SubscriberDao} from "./SubscriberDao";
import {Subscribers} from "./Subscribers";
import {Notification} from "./Notification";
import {Eap} from "./Eap";

const axios = require('axios');
const cron = require('node-cron');
const {WebClient} = require('@slack/web-api');
const {createEventAdapter} = require('@slack/events-api');
const express = require('express');
const currentPath = process.cwd();
const simpleGit = require('simple-git');
var git = simpleGit(currentPath);
const fs = require('fs');


const WATCHER_APP = 'https://jira-version-watcher.herokuapp.com/self/ping';

const eapDao = new EapDao();
const versionDao = new VersionDao();
const subscriberDao = new SubscriberDao();
const subscribers = new Subscribers();
const scanner = new Scanner();

function buildMessageWithText(text: string, href: string) {
    return [{
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": text
        },
        "accessory": {
            "type": "image",
            "image_url": "https://www.pngfind.com/pngs/m/432-4329261_jira-software-logo-jira-logo-hd-png-download.png",
            "alt_text": "plants"
        }
    },
        {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": " "
            },
        }];
}

function initEventsHandler(events) {
    events.on('message', (event) => {
        if (event.user == null) {
            return;
        }
        switch (event.text) {
            case 'newest':
                respondWithNewestVersionInfo(event);
                break;
            case 'eaps':
                respondWithEapsInfo(event);
                break;
            case 'sub':
                addUserToSubscribers(event);
                break;
            case 'unsub':
                removeUserFromSubscribers(event);
                break;
            case 'commit':
                testCommit();
                break;
            // case 'forcebuild':
            // case 'forceeap':
            //     notifyBuilder(event);
            //     break;
            default:
                const res = web.chat.postMessage({channel: event.channel, text: 'Me no speak bot-aliano'});
                break;
        }
    });
}

function selfPing() {
    axios.get(WATCHER_APP);
}

function selfPingResponse(req, res, next) {
    res.send('Alive');
}

async function deleteSubscriber(subscriber: string) {
    await subscriberDao.delete(subscriber);
}

async function notifyWatchers(reason: string) {
    let subscribers = await subscriberDao.getAll();
    for (let i = 0; i < subscribers.length; i++) {
        console.log('Notifying: ', subscribers[i]);
        const postPromise = web.chat.postMessage({
            channel: subscribers[i],
            blocks: buildMessageWithText(reason, "http://www.spartez.com")
        });
        postPromise.catch((error) => {
            console.log('unable to notify: ', subscribers[i]);
            console.log('Error is: ', error.data.error);
        });
    }
}

function notifyAll(notificationObject: Notification) {
    Promise.all([
        notifyWatchers(notificationObject.reason),
    ])
        .catch(error => console.log('unable to finish notifications, reason: ', error));
}

function buildMessageFromEaps(eaps) {
    const header = 'New JIRA Software EAP(s) released: \n\n';
    const content = eaps.map(eap => `${eap.version} released on ${eap.released}, download: ${eap.url}`)
        .reduce((a, b) => a + '\n' + b);
    const footer = `\n\nGlobal EAP URL: ${Scanner.EAPS_URL}`;
    return new Notification(header + content + footer, '');
}

async function checkEaps() {
    console.log('Getting latest JIRA Server EAP version...');
    const newestEaps = scanner.scanEaps();
    const latestEaps: Eap[] = await eapDao.getAll();
    const latestMd5 = latestEaps.map(eap => eap.eap);
    const newDetected = newestEaps.filter(newEap => !latestMd5.includes(newEap.eap));
    if (newDetected.length > 0) {
        console.log('Newer EAP RELEASE detected!');
        console.log('Notifying Molicki und SPG!');
        eapDao.setAll(newestEaps);
        const message = buildMessageFromEaps(newDetected);
        notifyAll(message);
    } else {
        console.log('No new JIRA Server EAP detected. Current is: ', latestEaps[0].version);
    }
}

async function checkVersion() {
    console.log('Getting latest JIRA Server version...');
    const newestVersion: string = scanner.scanVersion();
    const latestVersion = await versionDao.getVersion();
    if (newestVersion != latestVersion) {
        console.log('=============================================');
        console.log('Newer version detected!');
        console.log('Notifying Molicki und SPG!');
        notifyAll(new Notification('New JIRA Server released: ' + newestVersion + ' check it: ' + Scanner.JIRA_SERVER_RELEASES, 'buildlatestimage'));
        versionDao.setVersion(newestVersion);
        console.log('Current version: ', newestVersion);
        console.log('=============================================')
    } else {
        console.log('No new JIRA Server version detected. Current is: ' + latestVersion);
    }
}

async function respondWithEapsInfo(event) {
    const eapsInfo = await eapDao.getAll();
    web.chat.postMessage({
        channel: event.channel,
        text: 'Newest EAPS are: ' + eapsInfo.map((eap) => eap.eap + ' => ' + eap.url + '\n').join('\n')
    });
}

async function respondWithNewestVersionInfo(event) {
    const newestJiraVersion = await versionDao.getVersion();
    web.chat.postMessage({channel: event.channel, text: 'Newest JIRA is: ' + newestJiraVersion});
}

async function addUserToSubscribers(event) {
    let sourceId = event.channel;
    const isUserSubscriber = await subscribers.isSubscriber(sourceId);
    if (!isUserSubscriber) {
        await subscriberDao.insert(sourceId);
        web.chat.postMessage({channel: event.channel, text: 'Subscribed'});
    } else {
        web.chat.postMessage({channel: event.channel, text: 'Already subscribed'});
    }
}

async function removeUserFromSubscribers(event) {
    let sourceId = event.channel;
    await deleteSubscriber(sourceId);
    web.chat.postMessage({channel: event.channel, text: 'Unsubscribed'});
}

async function getLatestVersion(req, res, next) {
    const newestJiraVersion = await versionDao.getVersion();
    res.send(newestJiraVersion);
}

async function getLatestVersionDownloadUrl(req, res, next) {
    const newestJiraVersion = await versionDao.getVersion();
    let downloadUrl = 'https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-software-' + newestJiraVersion + '.tar.gz';
    res.send(downloadUrl);
}

function findTarForJiraSoftware(eaps) {
    return eaps.filter((eap) => eap.url.includes('jira-software'))
        .filter((eap) => eap.url.includes('tar.gz'))[0];
}

async function getEapTar() {
    const eapsInfo = await eapDao.getAll();
    return findTarForJiraSoftware(eapsInfo);
}

async function getLatestEapDownloadUrl(req, res, next) {
    let eapTar = await getEapTar();
    res.send(eapTar.url);
}

async function getLatestEapVersion(req, res, next) {
    const eapsInfo = await eapDao.getAll();
    const eaptar = findTarForJiraSoftware(eapsInfo);
    res.send(eaptar.version);
}

async function testCommit() {
    console.log('cloning...');
    git.clone('https://molickispartezsoftware:zq2zxjLDRstCnF37Fz5F@bitbucket.org/molickispartezsoftware/jiraversionwatcher.git').then(() => {
        const data = new Uint8Array(Buffer.from('NodeTest'));
        fs.writeFile('./jiraversionwatcher/message2.txt', data, (err) => {
            if (err) throw err;
            console.log('The file has beans saved!');
            git = simpleGit('jiraversionwatcher');
            git.addConfig('user.name', 'MolickiHeroku').addConfig('user.email', 'grzegorz.molicki@spartez-software.com');
            git.add(['message2.txt']);
            git.commit(`Commit Test`).then(() => console.log('committd')).catch(err => console.error('err while committing', err));
            git.push('origin', 'master').then(() => console.log('pushd')).catch(err => console.error('err while push', err));
        });
    }).catch(err => console.error('errord', err));
}

// ------ MAIN -------
const app = express();
const slackEvents = createEventAdapter(process.env.SIGNING_SECRET);
const web = new WebClient(process.env.ACCESS_TOKEN);

app.use('/slack/events', slackEvents.expressMiddleware());
app.use('/self/ping', selfPingResponse);
app.get('/version/latest', getLatestVersion);
app.get('/version/eap', getLatestEapVersion);
app.get('/version/eap/downloadUrl', getLatestEapDownloadUrl);
app.get('/version/latest/downloadUrl', getLatestVersionDownloadUrl);
app.listen(process.env.PORT, () => console.log(`Jira Version Watcher listening on port ${process.env.PORT}!`));
slackEvents.on('error', console.error);
initEventsHandler(slackEvents);
cron.schedule('* * * * *', () => {
    checkEaps();
    checkVersion();
});
cron.schedule('*/15 * * * *', () => {
    selfPing();
});

checkEaps();
