import {SubscriberDao} from "./SubscriberDao";

export class Subscribers {
    private subscriberDao : SubscriberDao;
    constructor() {
        this.subscriberDao = new SubscriberDao();
    }

    async isSubscriber(name : string) {
        return await this.subscriberDao.isPresent(name);
    }
}
