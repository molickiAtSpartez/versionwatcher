import {Dao} from "./Dao";

export class VersionDao extends Dao {

    setVersion(version : string) {
        this.cleanTable('versions');
        this.insertVersion(version);
    }

    async getVersion() {
        let query = 'SELECT version as version FROM versions LIMIT 1';
        const version = await this.select(query);
        return version[0].version;
    }

    private insertVersion(version : string) {
        try {
            this.getClient().query('BEGIN');
            try {
                this.getClient().query('INSERT INTO versions (version) VALUES(\'' + version + '\');');
                this.getClient().query('COMMIT')
            } catch (err) {
                console.log('err: ', err);
                this.getClient().query('ROLLBACK');
                throw err
            }
        } catch (e) {
            console.error(e);
        }
    }
}
