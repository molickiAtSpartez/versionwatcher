import {Eap} from "./Eap";
import {Dao} from "./Dao";

export class EapDao extends Dao {
    async getAll() {
        const query = "SELECT eap, url, version FROM eaps;";
        return this.select(query);
    }

    setAll(eaps: Eap[]) {
        this.cleanTable("eaps");
        this.insertEaps(eaps);
    }

    private insertEaps(eaps: Eap[]) {
        try {
            const query = 'INSERT INTO eaps(eap, url, version, released) VALUES($1, $2, $3, $4) RETURNING *';
            this.getClient().query('BEGIN');
            try {
                eaps.forEach((eap : Eap) => {
                    this.getClient().query(query, [eap.eap, eap.url, eap.version, eap.released]);
                });
                this.getClient().query('COMMIT');
            } catch (err) {
                console.log('err: ', err);
                this.getClient().query('ROLLBACK');
                throw err
            }
        } catch (e) {
            console.error(e);
        }
    }
}
