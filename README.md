### How it works
Every minute crawler fetches `JIRA_EAPS` and `JIRA_SERVER_VERSION` and confronts it against versions stored in DB (postgres on heroku).

If there's a new version detected it's stored in DB and a notification is pushed into `#spg-notification` channel and all the subscribers who successfully subscribed via direct message to versionchecker bot on slack.
Additionally, Docker image building service is notified via POST HTTP request, which then uses one of endpoints to retrieve valid version and package download url to build and push new dockerized JIRA Software.

### Bot commands (Spartez Slack -> versionchecker):
> sub

Subscribes to version direct notification list

> unsub

Unsubscribes from version direct notification list

> newest

Bot will respond with latest JIRA stable release version

> eaps

Bot will respond with latest JIRA early access builds hashes and download urls

### Bot endpoints

`/version/latest`

Used to retrieve current JIRA Software stable version

`/version/eap`

Used to retrieve current JIRA eap version (usually it's next JIRA version + build number)

`/version/eap/downloadUrl`

Used to retrieve JIRA Software tar.gz archive download url

`/version/latest/downloadUrl`

Used to retrieve JIRA Software latest stable version tar.gz archive download url

### Where is it hosted
Bot/version scanner itself runs as Heroku App
Docker image builder is a Go service running on Eos virtual machine


### How to recreate bot
Slack bot creation: https://github.com/slackapi/node-slack-sdk - Connect to preview 
 

### Access and handy tricks
Self-ping: Pings itself on heroku in order to prevent app going to sleep after 15 minutes on free heroku plan
Slack access: configured via environmental variables SIGNING_SECRET and ACCESS_TOKEN generated in Slack in Apps Directory → https://spartezsoftware.slack.com/apps/AS4QCNKCY-jiraversionwatcher  
 
### Releasing
Push to heroku origin https://git.heroku.com/jira-version-watcher.git, Heroku builds on push and releases new version.

 
### Frequently Failing Processes
 Sometimes Atlassian changes JSON structure (i.e. they provide the same MD5 hash for different packages). Other than that, no issues experienced since app introduction.
